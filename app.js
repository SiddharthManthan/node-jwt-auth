const express = require("express");
const mongoose = require("mongoose");
require("dotenv").config();
const authRoutes = require("./routes/authRoutes");
const cookieParser = require('cookie-parser');
const { requireAuth, checkUser } = require("./middleware/authMiddleware");

const app = express();

// middleware
app.use(express.static("public"));
app.use(express.json());
app.use(cookieParser());

// view engine
app.set("view engine", "ejs");

// database connection
const connectToDatabase = () => { 
    mongoose.connect(process.env.DBURI, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    })
    .then((result) => app.listen(process.env.JWTAUTHPORT))
    .catch((err) => {
        console.log(err)
        setTimeout(connectToDatabase, 5000)
    });
}

connectToDatabase()

// routes
// Todo: Apply a middleware to every route. This middleware modifies res object and it flows down 
app.get('*', checkUser);
app.get("/", (req, res) => res.render("home"));
app.get("/smoothies", requireAuth, (req, res) => res.render("smoothies"));
app.use(authRoutes);